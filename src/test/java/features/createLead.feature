Feature: Create Lead
#Scenario: Create Lead in LeafTabs 
#Given Open Browser
#And Launch TestLeaf URL
#And Enter UserName as DemoSalesManager
#And Enter Password as crmsfa
#And Click Login
#And Click CRMSFA
#And Click Leads
#And Click Create Leads
#And Enter Comapany Name as Accenture
#And Enter First Name as Sivaprakash
#And Enter Last Name as R
#When Click Create Lead
#Then Verify Create Lead is success




Scenario Outline: Create Lead in LeafTabs 

Given Enter UserName as DemoSalesManager
And Enter Password as crmsfa
And Click Login
And Click CRMSFA
And Click Leads
And Click Create Leads
And Enter Comapany Name as <CName>
And Enter First Name as <FName>
When Enter Last Name as <LName>
Then Click Create Lead
#Then Verify Create Lead is success

Examples:
|CName|FName|LName|
|Accenture|Sivaprakash|R|
|Infosys|Sangeetha|S|