package implementation;

import java.io.IOException;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase{
@Before
 public void beforeScenario(Scenario sc) {
	System.out.println(sc.getName());
	System.out.println(sc.getId());
	startReport();
	test=extent.createTest(sc.getName(), sc.getId());
	test.assignAuthor("Siva");
	test.assignCategory("Smoke");
	startApp("chrome", "http://leaftaps.com/opentaps/control/login");
	
}
@After
public void afterScenario(Scenario sc) {
	System.out.println(sc.getStatus());
	close();
	endReport();
}
}
