package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import utils.Report;

public class SeleniumBase extends Report implements Browser, Element{
	public int i=1;
	public static RemoteWebDriver driver;
	WebDriverWait wait;
	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+" clicked", "pass");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" clicked", "fail");
			throw new RuntimeException();
		}
//		finally {
//			takeSnap();
//		}
	}

	

	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The given data "+data+" is entered in the element "+ele, "pass");
		} catch (ElementNotInteractableException e) {
			reportStep("The element "+ele+" is not editable", "fail");
		}
		 catch (IllegalArgumentException e) {
			 reportStep("The element "+ele+" is not editable", "fail");
			}finally {
				takeSnap();
				}

	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			reportStep("The value is cleared in the element "+ele, "pass");
		} catch (InvalidElementStateException e) {
			reportStep("The value is cleared not in the element "+ele, "fail");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :"+data+" entered Successfully in the element "+ele, "pass");
		} catch (ElementNotInteractableException e) {
			reportStep("The Data :"+data+" was not entered  in the element "+ele, "fail");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
}

	@Override
	public String getElementText(WebElement ele) {
		try {
			String eleText = ele.getText();
			reportStep("The element"+ele+" text captured successfully", "pass");
			return eleText;
		} catch (Exception e) {
			reportStep("The element"+ele+" text was not captured", "fail");
			return null;
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		try {
			String colorValue = ele.getCssValue("background-color");
			reportStep("Color value of the element "+ele+" returned successfully", "pass");
			return colorValue;
		} catch (Exception e) {
			reportStep("Color value of the element "+ele+" was not identified", "fail");
			return null;
		}
		
		
	}

	@Override
	public String getTypedText(WebElement ele) {
		try {
			String text = ele.getText();
			reportStep("Text value of the element "+ele+" returned successfully", "pass");
			return text;
		} catch (Exception e) {
			reportStep("Text value of the element "+ele+" was not identified", "fail");
			return null;
		}
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
		wait=new WebDriverWait(driver, 10);
			Select lst=new Select(ele);
			lst.selectByVisibleText(value);
			reportStep("The Given value "+value+" is selected in the dropdown "+ele,"pass");
		} catch (NoSuchElementException e) {
			reportStep("The Dropdown "+ele+" is not found.","fail");
		}
		finally {
			takeSnap();
		}
		

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			wait=new WebDriverWait(driver, 10);
				Select lst=new Select(ele);
				lst.selectByIndex(index);
				reportStep("The drop down value for the given index "+index+" is selected in the element"+ele,"pass");
			} catch (NoSuchElementException e) {
				reportStep("The Dropdown "+ele+" is not found.","fail");
			}
			finally {
				takeSnap();
			}

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			wait=new WebDriverWait(driver, 10);
			Select lst=new Select(ele);
				lst.selectByValue(value);
				reportStep("The Given value "+value+" is selected in the dropdown "+ele,"pass");
			} catch (NoSuchElementException e) {
				reportStep("The Dropdown "+ele+" is not found.","fail");
			}
			finally {
				takeSnap();
			}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		String actualText = ele.getText();
		if (actualText==expectedText) {
			reportStep("The element text value matches with expected text value.","pass");
			return true;
		}
		else {
			reportStep("The element text value and expected text value are not same. The element text value is "+actualText+". The expected value is "+expectedText,"fail");
		return false;
		}
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String actualText = ele.getText();
		boolean contains = actualText.contains(expectedText);
		if (contains==true) {
			reportStep("The element text value contains expected text value.","pass");
			return true;
		}
		else {
			reportStep("The element text value does not contain the expected text value. The element text value is "+actualText+". The expected value is "+expectedText,"fail");
			return false;
		}
	}


	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		String attributeValue = ele.getAttribute(attribute);
		if (attributeValue==value) {
			reportStep("The exact given attribute value and expected value are same.","pass");
			return true;
		}
		else {
			reportStep("The exact given attribute value and expected value are different. The attribute value is "+attributeValue+". The expected value is "+value,"fail");
			return false;
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String attributeValue = ele.getAttribute(attribute);
		boolean contains = attributeValue.contains(value);
		if (contains==true) {
			reportStep("The exact given attribute value contains expected value","pass");
		}
		else {
			reportStep("The exact given attribute value doesnot contains expected value","fail");
		}
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if (displayed == true) {
				reportStep("The element " + ele + " is displayed in the DOM","pass");
			} else {
				reportStep("The element " + ele + " is not dispalyed in the DOM","fail");
			}
			return displayed;
		} finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if (displayed == true) {
				reportStep("The element " + ele + " is displayed in the DOM","pass");
				return false;
			} else {
				reportStep("The element " + ele + " is not dispalyed in the DOM","fail");
				return true;
			} 
		} finally {
			takeSnap();
		}
			}

	@Override
	public boolean verifyEnabled(WebElement ele) {
	try {
		boolean enabled = ele.isEnabled();
		if (enabled == true) {
			reportStep("The element " + ele + " is enabled","pass");
		} else {
			reportStep("The element " + ele + " is not enabled","fail");
		}
		return enabled;
	} finally {
		takeSnap();
	}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if (selected == true) {
				reportStep("The element " + ele + " is selected","pass");
			} else {
				reportStep("The element " + ele + " is not selected","fail");
			}
			return selected;
		} finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String url) {
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The URL: "+url+" loaded successfully in chrome", "pass");
		} catch (Exception e) {
			reportStep("Unable to launch URL in the given browser","fail");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) 
			{
				  System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				 ChromeOptions op = new   ChromeOptions();
				 op.addArguments("--disable-notifications");
				  driver = new ChromeDriver(op);
		
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver","./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver","./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The URL: "+url+" loaded successfully in "+browser, "pass");
		} catch (Exception e) {
			reportStep("Unable to launch URL in the given browser","fail");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator:"+locatorType+" Not Found with value: "+value,"fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			System.out.println("The Element is with Id "+value+" is found");
			return driver.findElementById(value);
		} catch (NoSuchElementException e) {
			reportStep("The Element is with Id "+value+" is not found","fail");
			throw new RuntimeException();
		}
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		
			try {
				switch (type.toLowerCase()) {
				case "class":
					List<WebElement> elementsByClass = driver.findElementsByClassName(value);
					return elementsByClass;
				case "id":
					List<WebElement> elementsById = driver.findElementsById(value);
					return elementsById;
				case "name":
					List<WebElement> elementsByName = driver.findElementsByName(value);
					return elementsByName;
				case "link":
					List<WebElement> elementsByLink = driver.findElementsByLinkText(value);
					return elementsByLink;
				case "xpath":
					List<WebElement> elementsByXpath = driver.findElementsByXPath(value);
					return elementsByXpath;
				}
			} catch (Exception e) {
				reportStep("Unable to find elements with locator "+type+" and value "+value, "fail");
			}
			finally {
				takeSnap();
			}
			return null;
		} 
	

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
			reportStep("Switched to alert successfully.","pass");
		} catch (NoAlertPresentException e) {
			reportStep("No alert present","fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		reportStep("Accepted alert successfully.","pass");
		} catch (NoAlertPresentException e) {
			reportStep("No alert present/Unable to accept alert","fail");
		}finally {
			takeSnap();
			}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Dismissed alert successfully.","pass");
		} catch (NoAlertPresentException e) {
			reportStep("No alert present/unable to dismiss alert","fail");
		}finally {
			takeSnap();
			}

	}

	@Override
	public String getAlertText() {
		try {
			String text = driver.switchTo().alert().getText();
			reportStep("The alert text is: "+text,"pass");
			return text;
			
		} catch (NoAlertPresentException e) {
			reportStep("No alert present/Unable to retrive alert text","fail");
			return null;
		}finally {
			takeSnap();
			}
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			reportStep("The given data is entered in the alert","pass");
		} catch (NoAlertPresentException e) {
			reportStep("No alert present /Unable to enter data in the alert","fail");
		}finally {
			takeSnap();
			}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindow = driver.getWindowHandles();
			List<String> lstWindow=new ArrayList<>(allWindow);
			String windowRef = lstWindow.get(index-1);
			driver.switchTo().window(windowRef);
			reportStep("Switched to the given window with index value: "+index+" successfully","pass");
		} catch (NoSuchWindowException e) {
			reportStep("No window with index value "+index+"found","fail");
		}
		finally {
			takeSnap();
			}
	}

	@Override
	public void switchToWindow(String title) {
		try {
			Set<String> allWindow = driver.getWindowHandles();
			List<String> lstWindow=new ArrayList<>(allWindow);
			for (int i=0;i<lstWindow.size();i++) {
				String windowRef = lstWindow.get(0);
				driver.switchTo().window(windowRef);
				if(driver.getTitle()==title) {
					break;
				}
			}
			reportStep("Switched to the given window with title: "+title+" successfully","pass");
		} catch (NoSuchWindowException e) {
			reportStep("No window with title "+title+"found","fail");
		}
		finally {
			takeSnap();
			}
	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			reportStep("Switched to frame"+index+" successfully.","pass");
		} catch (NoSuchFrameException e) {
			reportStep("No frame with index value "+index+" exists/unable to switch to specific frame","fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("Switched to the frame "+ele+" successfully.","pass");
		} catch (NoSuchFrameException e) {
			reportStep("No frame exists/unable to switch to specific frame","fail");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			reportStep("Switched to the frame with Name/Id "+idOrName+" successfully.","pass");
		} catch (NoSuchFrameException e) {
			reportStep("No frame exists with ID/Name: "+idOrName+"/unable to switch to specific frame","fail");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			reportStep("Switched to first frame/top window","pass");
		} catch (Exception e) {
			reportStep("Unable to switch to first frame/top window","fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyUrl(String url) {
		try {
			String actualUrl = driver.getCurrentUrl();
			if (actualUrl.equalsIgnoreCase(url)) {
				reportStep("The expected and current URL are same.","pass");
				return true;
			} else {
				reportStep("The expected and current title are different. The actual title is: " + actualUrl + " and the expected title is: " + url,"fail");
				return false;
			} 
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String title) {
		try {
			String actualTitle = driver.getTitle();
			if (actualTitle.equalsIgnoreCase(title)) {
				reportStep("The expected and current title are same.","pass");
				return true;
			} else {
				reportStep("The expected and current title are different.","fail");
				return false;
			} 
		} finally {
			takeSnap();
		}
	}
	
	@Override
	public boolean verifyTitleContains(String title) {
		try {
			String actualTitle = driver.getTitle();
			if (actualTitle.contains(title)) {
				reportStep("The title contains given value","pass");
				return true;
			} else {
				reportStep("The title doesnot contains given value","fail");
				return false;
			} 
		} finally {
			takeSnap();
		}	
	}
//
	@Override
	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File dest=new File("./snaps/"+testCaseName+""+i+".png");
			FileUtils.copyFile(src, dest);
		} catch (WebDriverException e) {
			reportStep("Unable to take screen prints","fail");
		} catch (IOException e) {
			reportStep("Unable to take screen prints","fail");
		}
		i++;

	}

	@Override
	public void close() {
		try {
			driver.close();
			reportStep("Browser closed successfully", "pass");
		} catch (Exception e) {
			reportStep("Unable to close browser", "fail");
		}

	}

	@Override
	public void quit() {
		try {
			driver.quit();
			reportStep("Browser task killed successfully", "pass");
		} catch (Exception e) {
			reportStep("Unable to kill browser task", "fail");
		}

	}



	

}
