package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;

import utils.Annotations;



public class TC001_LoginAndLogout extends Annotations{
	@BeforeTest
	public void setData() {
		testCaseName="TC001_LoginAndLogout";
		testCaseDesc="Login and Logout of TestLeaf";
		author="Siva";
		category="Smoke";
		excelFilePath="TC001";
	}
	@Test(dataProvider="fetchData")
	public void LoginAndLogout(String UserName, String Password) {
		new LoginPage()
		.enterUserName(UserName)
		.enterPassword(Password)
		.clickLogin()
		.clickLogout();
	}
	
}
