package com.yalla.testcases;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;

import utils.Annotations;



public class TC002_CreateLead extends Annotations{
	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testCaseDesc="Create Lead";
		author="Siva";
		category="Smoke";
		excelFilePath="TC002";
	}
	@Test(dataProvider="fetchData")
	public void createLead(String UserName, String Password, String CompanyName, String FirstName, String LastName) {
		new LoginPage()
		.enterUserName(UserName)
		.enterPassword(Password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLeads()
		.enterCompanyName(CompanyName)
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.clickCreateLead()
		.verifyFirstName(FirstName);
	}
}
