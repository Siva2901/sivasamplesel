package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import utils.Annotations;



public class LoginPage extends Annotations{
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="username") WebElement eleUserName;
	@FindBy(how=How.ID, using="password") WebElement elePassword;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	
	@And("Enter UserName as (.*)")
	public LoginPage enterUserName(String data) {
		clearAndType(eleUserName, data);
		return this;
	}
	
	@And("Enter Password as (.*)")
	public LoginPage enterPassword(String data) {
		clearAndType(elePassword, data);
		return this;
	}
	@And("Click Login")
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
	
}
