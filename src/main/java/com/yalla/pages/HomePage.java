package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import utils.Annotations;



public class HomePage extends Annotations{
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") WebElement eleCRMSFA;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	
	public LoginPage clickLogout() {
		click(eleLogout);
		return new LoginPage();
	}
	
	@And("Click CRMSFA")
	public MyHomePage clickCRMSFA() {
		click(eleCRMSFA);
		return new MyHomePage();
	}

}
