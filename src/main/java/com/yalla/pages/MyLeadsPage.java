package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import utils.Annotations;





public class MyLeadsPage extends Annotations{
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
	
	@And("Click Create Leads")
	public CreateLeadPage clickCreateLeads() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
}
