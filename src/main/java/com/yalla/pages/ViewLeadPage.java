package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import utils.Annotations;



public class ViewLeadPage extends Annotations{
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement eleFirstName;
	
	@And("Verify Create Lead is success")
	public ViewLeadPage verifyFirstName(String FirstName) {
		String FirstNameText = getElementText(eleFirstName);
		if(FirstName.equalsIgnoreCase(FirstNameText)) {
			reportStep("Create lead is successful", "pass");
		}
		else {
			reportStep("Create lead is failed", "fail");
		}
		return this;
	}
}
