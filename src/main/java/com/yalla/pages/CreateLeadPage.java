package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import utils.Annotations;


public class CreateLeadPage extends Annotations{
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//input[@name='submitButton']") WebElement eleCreateLead;
	
	@And("Enter Comapany Name as (.*)")
	public CreateLeadPage enterCompanyName(String CompanyName) {
		clearAndType(eleCompanyName, CompanyName);
		return this;
	}
	
	@And("Enter First Name as (.*)")
	public CreateLeadPage enterFirstName(String FirstName) {
		clearAndType(eleFirstName, FirstName);
		return this;
	}
	
	@And("Enter Last Name as (.*)")
	public CreateLeadPage enterLastName(String LastName) {
		clearAndType(eleLastName, LastName);
		return this;
	}
	
	@And("Click Create Lead")
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new ViewLeadPage();
	}
	
	
}
