package projectDay2701;



import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AcmeSite {
	
	@Test(dataProvider="fetchData")
	public void classWork(String taxId) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.findElement(By.id("email")).sendKeys("siva.rathinam1993@gmail.com");
		driver.findElementById("password").sendKeys("Ariessiva@93");
		driver.findElementById("buttonLogin").click();
		Thread.sleep(3000);
		WebElement eleVendor = driver.findElementByXPath("//button[text()=' Vendors']");
		Actions builder=new Actions(driver);
		builder.moveToElement(eleVendor).click().perform();
		driver.findElementByXPath("//a[text()='Search for Vendor']").click();
		driver.findElementById("vendorTaxID").sendKeys(taxId);
		driver.findElementById("buttonSearch").click();
		String VendorName = driver.findElementByXPath("//table[@class='table']//td").getText();
		System.out.println(VendorName);
		driver.close();
	}
	
	@DataProvider(name="fetchData")
	public static Object[] fetchData() throws IOException {
		
		Object[] data= new Object[2];
		data[0]="RU567434";
		data[1]="FR453231";
		return data;

}
}
