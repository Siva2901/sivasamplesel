package utils;

import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

import utils.DataLibrary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase {
	@DataProvider(name="fetchData")
	public Object[][] fetchData() throws IOException {
		return DataLibrary.fetchData(excelFilePath);
	}
  @Parameters ({"url"})
  @BeforeMethod(groups="any")
  public void beforeMethod(String url) {
	  	startApp("chrome", url);
		
		
  }

  @AfterMethod(groups="any")
  public void afterMethod() {
	 close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite(groups="any")
  public void afterSuite() {
	  quit();
  }

}
