package utils;

import java.io.IOException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Report {
	public static ExtentHtmlReporter reporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	public String testCaseName, testCaseDesc, author, category, excelFilePath;
	@BeforeSuite(groups="any")
	 public void startReport(){
		  reporter=new ExtentHtmlReporter("./reports/result.html");
		  reporter.setAppendExisting(true);
		  extent=new ExtentReports();
		 extent.attachReporter(reporter);
		 
	}
	@BeforeClass(groups="any")
	public void reportTestCaseDetail() throws IOException {
		test=extent.createTest(testCaseName, testCaseDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
		
		 }

	@AfterSuite(groups="any")
	public void endReport() {
		extent.flush();
	}
	public void reportStep(String desc, String status) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}else if (status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		}
	}
}
